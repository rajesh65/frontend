import React from 'react'
import '../App.css';
import { Link } from "react-router-dom"
export default function Nav() {

    const navstyle={
        color:'white'
    };
  return (
    
        <nav>
            <h3>logo</h3>
            <ul className='nav-links'>
            <Link style={navstyle} to='/login'>
                <li>Login</li>
            </Link>
            <Link style={navstyle} to='/register'>
                <li>Register</li>
            </Link>
            </ul>
        </nav>
    
  )
}
