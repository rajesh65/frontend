import React, { useState } from 'react'
import axios from "axios";
import '../App.css';

export default function Register() {
    const [Name, setName] = useState(null);
    const [Email, setEmail] = useState(null);
    const [Phone, setPhone] = useState(null);
    const [Password, setPassword] = useState(null);

    const handlesubmit = async () => {
        console.log("register");
        const data = {
            Name: Name,
            Email: Email,
            phone: Phone,
            password: Password
        }
        console.log(data, "data")
      await  axios.post("http://localhost:2202/insert", data).then((response) => {
            console.log(response, "response");
        }).catch((error) => {
            console.log(error, "error")
        })
    }
    return (
        <div>
            <div class='register'>
                <h1>Register</h1>
                <br />
                <label>Name</label>
                <br />
                <input type="text"
                    placeholder='Name'
                    name="Name"
                    onChange={(e) => { setName(e.target.value) }}
                />
                <br />
                <label>Email</label>
                <br />
                <input type="text"
                    placeholder='Email'
                    name="Email"
                    onChange={(e) => { setEmail(e.target.value) }}
                />
                <br />
                <label>Phone</label>
                <br />
                <input type="text"
                    placeholder='Phone'
                    name='Phone'
                    onChange={(e) => { setPhone(e.target.value) }}
                />
                <br />
                <label>Password</label>
                <br />
                <input type="pasword"
                    placeholder='Password'
                    name='password'
                    onChange={(e) => { setPassword(e.target.value) }}
                />
                <br />
                <button className='cls-button' onClick={handlesubmit}>Register</button>
            </div>
        </div>
    )
}
