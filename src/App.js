import "../src/App.css"
import Login from './Components/Login';
import Register from './Components/Register';
import Nav from "./Components/Nav"
import {BrowserRouter as Router,Switch,Route} from "react-router-dom"
function App() {
  return (
   
      
      
     <Router>
     <div className="App">
     <Nav/>
     <Switch>
       <Route  path="/" exact component={Home}/>
       <Route path="/login" component={Login}/>
       <Route  path="/register" component={Register}/>
     </Switch>
     </div>
     </Router>

      

         
  );
}
const Home =()=>{
  return(<div>
    <h1>Home </h1>
  </div>)
}
export default App;
